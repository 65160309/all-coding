
public class Cinema {
    private String[][] stringTable;
    private String[] x;
    private String[] y;
    private String num;

    public Cinema(String[][] stringTable, String[] x, String[] y) {
        this.stringTable = stringTable;
        this.x = x;
        this.y = y;
    }

    public String[][] createTable(String[] x, String[] y) {
        for (int i = 0; i < x.length; i++) {
            for (int j = 0; j < y.length; j++) {
                stringTable[i][j] = x[i] + y[j];
            }
        }
        return stringTable;
    }

    public boolean selectChair(String num) {
        for (int i = 0; i < x.length; i++) {
            for (int j = 0; j < y.length; j++) {
                if (num.equals(stringTable[i][j])) {
                    stringTable[i][j] = "X";
                    return true;
                } else if (stringTable[i][j].equals("X")) {
                    return false;
                }
            }
        }
        return true;
    }

    public void setStringTable(String[][] stringTable) {
        this.stringTable = stringTable;
    }

    public String[] getX() {
        return this.x;
    }

    public void setX(String[] x) {
        this.x = x;
    }

    public String[] getY() {
        return this.y;
    }

    public void setY(String[] y) {
        this.y = y;
    }

}
