import java.util.Scanner;

public class Movies {
    private Cinema c;
    private String[][] stringTable = new String[4][6];
    private String[] x = { "A", "B", "C", "D" };
    private String[] y = { "1", "2", "3", "4", "5", "6" };

    public void newCinema() {
        c = new Cinema(stringTable, x, y);
    }

    public void show() {
        newCinema();
        while (true) {
            printTable();
            checkSeat();
        }

    }

    public void printTable() {
        String[][] t = c.createTable(x, y);
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 6; j++) {
                System.out.print(t[i][j] + " ");
            }
            System.out.println();
        }
    }

    public void checkSeat() {
        Scanner kb = new Scanner(System.in);

        while (true) {
            System.out.print("Please select Seat:");
            String num = kb.next();
            if (c.selectChair(num) == false) {
                System.out.println("Invalid");
                continue;
            }
            c.selectChair(num);
            break;
        }

    }

}
