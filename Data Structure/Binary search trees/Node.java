public class Node {
    public int key;
    public double data;
    public Node next;
    public Node RightChild;
    public Node LeftChild;

    public Node(int key, double data) {
        this.key = key;
        this.data = data;
    }

    public void displayNode() {
        System.out.println("Key = " + key + " Data = " + data);
    }
}
